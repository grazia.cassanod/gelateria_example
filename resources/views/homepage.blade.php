<x-layout>

    <header class="header">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                    <div class="col-12 col-md-6 text-white text-shadow">
                        <p class="lead font-italic mt-5 mt-md-3">Voglia di gelato?</p>
                        <h1 class="display-4 my-5">Gelateria</h1>
                        <p class="lead">Questo è il punto nel quale inserire la nostra USP, per far capire che la soluzione che si cercava è <i>proprio</i> quella che offriamo!</p>
                    </div>
                <div class="col-12 text-center text-lg-right">
                    <a id="headerCTA" href="" class="btn btn-lg text-white bg-yellow rounded-pill px-3 shadow text-uppercase">prenota adesso</a>
                </div>
            </div>
        </div>
    </header>

    <!-- fase di attenzione -->
    <div class="container my-5 py-5">
        <div class="row">
            <div class="col-12 col-md-8">
                <h2>I vantaggi generici del gelato</h2>
                <p class="lead text-secondary">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sunt libero ipsam repellendus qui atque aspernatur sed impedit voluptatem eius harum vitae, veniam consectetur assumenda ipsum, labore laudantium sint? Minus, iste.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-4 mb-3">
                <div class="over-box">
                    <p class="lead text-secondary text-center bg-white w-100 over-text">Motivazione</p>
                    <img src="/imgs/box1.jpg" class="img-fluid over-img" alt="">
                </div>
            </div>

            <div class="col-12 col-md-4 mb-3">
                <div class="over-box">
                    <p class="lead text-secondary text-center bg-white w-100 over-text">Motivazione</p>
                    <img src="/imgs/box2.jpg" class="img-fluid over-img" alt="">
                </div>
            </div>

            <div class="col-12 col-md-4 mb-3">
                <div class="over-box">
                    <p class="lead text-secondary text-center bg-white w-100 over-text">Motivazione</p>
                    <img src="/imgs/box3.jpg" class="img-fluid over-img" alt="">
                </div>
            </div>

        </div>
    </div>

    <div class="container-fluid my-5 py-5 bg-section-cta">
        <div class="row h-100 justify-content-center align-items-center">
            <div class="col-8 text-center text-white">
                <p class="h1 text-shadow">Altro invito all'azione!</p>
            </div>
            <div class="col-12 text-center">
                <a href="" class="btn px-4 btn-lg rounded-pill text-uppercase bg-yellow shadow text-white">prenota</a>
            </div>
        </div>
    </div>

    <p style="height: 2000px;"></p>

</x-layout>