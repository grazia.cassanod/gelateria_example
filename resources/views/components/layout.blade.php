<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gelateria</title>
    <link rel="stylesheet" href="/css/app.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&family=Petit+Formal+Script&display=swap" rel="stylesheet">
</head>
<body>
    
    <x-_navbar />

    {{$slot}}

    <x-_footer />
    
    <script src="/js/app.js"></script>

</body>
</html>